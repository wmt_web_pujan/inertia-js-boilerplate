<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use \App\Model\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['email'=>'test@gmail.com','password'=>'test@091283'],
        ];

        foreach ($data as $value) {
            $admin = Admin::updateOrCreate([
                'email'=>$value['email'],
                'password'=>Hash::make($value['password']),
            ]);

            if($admin){
                $admin->assignRole('super admin');
            }
        }
    }
}
