<?php

use Illuminate\Database\Seeder;
use \App\Model\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['first_name'=>'suresh','last_name'=>'singh','email'=>'suresh@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'prank','last_name'=>'patel','email'=>'prank@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'user','last_name'=>'patel','email'=>'user@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'john','last_name'=>'patel','email'=>'john@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit1@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohi2@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit3@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit4@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit5@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit6@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit7@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit8@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit9@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit10@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit11@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit12@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit13@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit14@gamil.com','password' => 'user@091283','status' => 1],
            ['first_name'=>'rohit','last_name'=>'patel','email'=>'rohit15@gamil.com','password' => 'user@091283','status' => 1],
        ];

        foreach ($data as $value) {
            User::updateOrCreate(['email' => $value['email']],[
                'first_name'=>$value['first_name'],
                'last_name'=>$value['last_name'],
                'email'=>$value['email'],
                'password'=>\Illuminate\Support\Facades\Hash::make($value['password']),
            ]);
        }
    }
}
