(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./public/assets/general/User_placeholder.svg":
/*!****************************************************!*\
  !*** ./public/assets/general/User_placeholder.svg ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/User_placeholder.svg?88da3e1635fbc4db5876dfb371296b62";

/***/ }),

/***/ "./public/assets/general/webmob-logo.svg":
/*!***********************************************!*\
  !*** ./public/assets/general/webmob-logo.svg ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/webmob-logo.svg?597e5674c4fc9a6dfdbeca0fc34e25d7";

/***/ }),

/***/ "./resources/js/Shared/AdminLayout/AdminSider.jsx":
/*!********************************************************!*\
  !*** ./resources/js/Shared/AdminLayout/AdminSider.jsx ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/lib/menu/style */ "./node_modules/antd/lib/menu/style/index.js");
/* harmony import */ var antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/menu */ "./node_modules/antd/lib/menu/index.js");
/* harmony import */ var antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/lib/layout/style */ "./node_modules/antd/lib/layout/style/index.js");
/* harmony import */ var antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var antd_lib_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd/lib/layout */ "./node_modules/antd/lib/layout/index.js");
/* harmony import */ var antd_lib_layout__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(antd_lib_layout__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");





function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var Sider = antd_lib_layout__WEBPACK_IMPORTED_MODULE_3___default.a.Sider;
var SubMenu = antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.SubMenu;

function AdminSider(props) {
  var _admin$role, _admin$role2, _admin$role3, _admin$role4, _admin$role5, _admin$role6, _admin$role7;

  var admin = props.admin;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      isDesktop = _useState2[0],
      setIsDesktop = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      collapsed = _useState4[0],
      setCollapsed = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(null),
      _useState6 = _slicedToArray(_useState5, 2),
      activeMenu = _useState6[0],
      setActiveMenu = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(null),
      _useState8 = _slicedToArray(_useState7, 2),
      activeMainMenu = _useState8[0],
      setActiveMainMenu = _useState8[1];

  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    var width = window.innerWidth;
    setIsDesktop(width);
    setActiveMenu(route().current());
    setActiveMainMenu(route().current().split(".")[0]);
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    window.addEventListener("resize", handleResize);
    return function () {
      return window.removeEventListener("resize", handleResize);
    };
  });
  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    setCollapsed(props.collapsed);
  }, [props.collapsed]);

  var handleResize = function handleResize() {
    setIsDesktop(window.innerWidth);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement("div", {
    className: "sider_color"
  }, isDesktop > 991.98 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(Sider, {
    collapsible: true,
    collapsed: collapsed,
    trigger: null
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a, {
    theme: "dark",
    defaultSelectedKeys: [activeMenu],
    defaultOpenKeys: [activeMainMenu],
    mode: "inline"
  }, admin !== null && admin !== void 0 && (_admin$role = admin.role) !== null && _admin$role !== void 0 && _admin$role.map(function (x) {
    return x.name;
  }).includes("dashboard") || admin !== null && admin !== void 0 && (_admin$role2 = admin.role) !== null && _admin$role2 !== void 0 && _admin$role2.map(function (x) {
    return x.name;
  }).includes("super admin") ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
    key: "dashboard",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_6__["PieChartOutlined"], null)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__["InertiaLink"], {
    href: route("dashboard")
  }, "DashBoard")) : null, admin !== null && admin !== void 0 && (_admin$role3 = admin.role) !== null && _admin$role3 !== void 0 && _admin$role3.map(function (x) {
    return x.name;
  }).includes("user") || admin !== null && admin !== void 0 && (_admin$role4 = admin.role) !== null && _admin$role4 !== void 0 && _admin$role4.map(function (x) {
    return x.name;
  }).includes("super admin") ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
    key: "user",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_6__["UserOutlined"], null)
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__["InertiaLink"], {
    href: route("user")
  }, "User")) : null, (admin === null || admin === void 0 ? void 0 : (_admin$role5 = admin.role) === null || _admin$role5 === void 0 ? void 0 : _admin$role5.map(function (x) {
    return x.name;
  }).includes("super admin")) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(SubMenu, {
    key: "acl",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_6__["TeamOutlined"], null),
    title: "Manage Role"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
    key: "acl.role"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__["InertiaLink"], {
    href: route("acl.role")
  }, "Role")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
    key: "acl.admin"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_5__["InertiaLink"], {
    href: route("acl.admin")
  }, "Admin"))), admin !== null && admin !== void 0 && (_admin$role6 = admin.role) !== null && _admin$role6 !== void 0 && _admin$role6.map(function (x) {
    return x.name;
  }).includes("files") || admin !== null && admin !== void 0 && (_admin$role7 = admin.role) !== null && _admin$role7 !== void 0 && _admin$role7.map(function (x) {
    return x.name;
  }).includes("super admin") ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
    key: "9",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_6__["FileOutlined"], null)
  }, "Files") : null)) : "");
}

/* harmony default export */ __webpack_exports__["default"] = (AdminSider);

/***/ }),

/***/ "./resources/js/Shared/AdminLayout/Footer.jsx":
/*!****************************************************!*\
  !*** ./resources/js/Shared/AdminLayout/Footer.jsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Footer; });
/* harmony import */ var antd_lib_row_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/lib/row/style */ "./node_modules/antd/lib/row/style/index.js");
/* harmony import */ var antd_lib_row_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(antd_lib_row_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_row__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/row */ "./node_modules/antd/lib/row/index.js");
/* harmony import */ var antd_lib_row__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_row__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd_lib_col_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/lib/col/style */ "./node_modules/antd/lib/col/style/index.js");
/* harmony import */ var antd_lib_col_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd_lib_col_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var antd_lib_col__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd/lib/col */ "./node_modules/antd/lib/col/index.js");
/* harmony import */ var antd_lib_col__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(antd_lib_col__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);






function Footer() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(antd_lib_row__WEBPACK_IMPORTED_MODULE_1___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_3___default.a, {
    xs: 24,
    sm: 24,
    lg: 7,
    style: {
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement("span", null, "Copyright \xA9 ", new Date().getFullYear(), " Admin ")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_3___default.a, {
    xs: 24,
    sm: 24,
    lg: 10,
    style: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__["InertiaLink"], {
    href: "#",
    style: {
      margin: "0 15px"
    }
  }, "Privacy Policy"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__["InertiaLink"], {
    href: "#",
    style: {
      margin: "0 15px"
    }
  }, "Terms of Service"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__["InertiaLink"], {
    href: "#",
    style: {
      margin: "0 15px"
    }
  }, "Cookies Policy")));
}

/***/ }),

/***/ "./resources/js/Shared/AdminLayout/Header.jsx":
/*!****************************************************!*\
  !*** ./resources/js/Shared/AdminLayout/Header.jsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var antd_lib_drawer_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/lib/drawer/style */ "./node_modules/antd/lib/drawer/style/index.js");
/* harmony import */ var antd_lib_drawer_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(antd_lib_drawer_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_drawer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/drawer */ "./node_modules/antd/lib/drawer/index.js");
/* harmony import */ var antd_lib_drawer__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_drawer__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd_lib_button_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/lib/button/style */ "./node_modules/antd/lib/button/style/index.js");
/* harmony import */ var antd_lib_button_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd_lib_button_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var antd_lib_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd/lib/button */ "./node_modules/antd/lib/button/index.js");
/* harmony import */ var antd_lib_button__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(antd_lib_button__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var antd_lib_row_style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd/lib/row/style */ "./node_modules/antd/lib/row/style/index.js");
/* harmony import */ var antd_lib_row_style__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(antd_lib_row_style__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var antd_lib_row__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! antd/lib/row */ "./node_modules/antd/lib/row/index.js");
/* harmony import */ var antd_lib_row__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(antd_lib_row__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var antd_lib_col_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! antd/lib/col/style */ "./node_modules/antd/lib/col/style/index.js");
/* harmony import */ var antd_lib_col_style__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(antd_lib_col_style__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var antd_lib_col__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd/lib/col */ "./node_modules/antd/lib/col/index.js");
/* harmony import */ var antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(antd_lib_col__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd_lib_typography_style__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd/lib/typography/style */ "./node_modules/antd/lib/typography/style/index.js");
/* harmony import */ var antd_lib_typography_style__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(antd_lib_typography_style__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var antd_lib_typography__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd/lib/typography */ "./node_modules/antd/lib/typography/index.js");
/* harmony import */ var antd_lib_typography__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(antd_lib_typography__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! antd/lib/menu/style */ "./node_modules/antd/lib/menu/style/index.js");
/* harmony import */ var antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(antd_lib_menu_style__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var antd_lib_menu__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd/lib/menu */ "./node_modules/antd/lib/menu/index.js");
/* harmony import */ var antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _ant_design_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ant-design/icons */ "./node_modules/@ant-design/icons/es/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _public_assets_general_User_placeholder_svg__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../public/assets/general/User_placeholder.svg */ "./public/assets/general/User_placeholder.svg");
/* harmony import */ var _public_assets_general_User_placeholder_svg__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_public_assets_general_User_placeholder_svg__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _public_assets_general_webmob_logo_svg__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../../../public/assets/general/webmob-logo.svg */ "./public/assets/general/webmob-logo.svg");
/* harmony import */ var _public_assets_general_webmob_logo_svg__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_public_assets_general_webmob_logo_svg__WEBPACK_IMPORTED_MODULE_16__);













function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }




var SubMenu = antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.SubMenu;
var Title = antd_lib_typography__WEBPACK_IMPORTED_MODULE_9___default.a.Title,
    Text = antd_lib_typography__WEBPACK_IMPORTED_MODULE_9___default.a.Text;



function Header(props) {
  var admin = props.admin;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_12__["useState"])(null),
      _useState2 = _slicedToArray(_useState, 2),
      isDesktop = _useState2[0],
      setIsDesktop = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_12__["useState"])(false),
      _useState4 = _slicedToArray(_useState3, 2),
      visible = _useState4[0],
      setVisible = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_12__["useState"])(true),
      _useState6 = _slicedToArray(_useState5, 2),
      collapsed = _useState6[0],
      setCollapsed = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_12__["useState"])(null),
      _useState8 = _slicedToArray(_useState7, 2),
      activeMenu = _useState8[0],
      setActiveMenu = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_12__["useState"])(null),
      _useState10 = _slicedToArray(_useState9, 2),
      activeMainMenu = _useState10[0],
      setActiveMainMenu = _useState10[1];

  var showDrawer = function showDrawer() {
    setVisible(true);
  };

  var onClose = function onClose() {
    setVisible(false);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_12__["useEffect"])(function () {
    var width = window.innerWidth;
    setIsDesktop(width);
    setActiveMenu(route().current());
    setActiveMainMenu(route().current().split(".")[0]);
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_12__["useEffect"])(function () {
    window.addEventListener("resize", handleResize);
    return function () {
      return window.removeEventListener("resize", handleResize);
    };
  });

  var handleResize = function handleResize() {
    setIsDesktop(window.innerWidth);
  };

  var handleCollapsed = function handleCollapsed() {
    setCollapsed(!collapsed);
    props.handleCollapsed(collapsed);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", null, isDesktop > 991.98 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_row__WEBPACK_IMPORTED_MODULE_5___default.a, {
    type: "flex",
    justify: "center",
    align: "middle",
    className: "header_bg_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default.a, {
    span: 24,
    style: {
      padding: "0px 20px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    className: "app_header"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_row__WEBPACK_IMPORTED_MODULE_5___default.a, {
    type: "flex",
    justify: "middle",
    align: "space-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default.a, {
    style: {
      height: "auto",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    style: {
      width: "182px",
      color: "white"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("img", {
    src: _public_assets_general_webmob_logo_svg__WEBPACK_IMPORTED_MODULE_16___default.a,
    className: "app_logo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    onClick: handleCollapsed,
    style: {
      cursor: "pointer"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["MenuFoldOutlined"], {
    style: {
      fontSize: "20px",
      color: "white"
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default.a, {
    className: "app_menu_wrapper",
    style: {
      height: "auto"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    className: "app_menu"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a, {
    mode: "horizontal",
    triggerSubMenuAction: "click",
    style: {
      lineHeight: "0px",
      backgroundColor: "transparent"
    },
    defaultSelectedKeys: [activeMenu],
    defaultOpenKeys: [activeMainMenu]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(SubMenu, {
    style: {
      margin: "0px 10px"
    },
    title: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
      style: {
        display: "flex",
        alignItems: "center"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
      style: {
        position: "relative",
        width: "35px",
        height: "35px",
        overflow: "hidden",
        borderRadius: "50%",
        display: "flex",
        padding: "2px"
      }
    }, admin !== null && admin !== void 0 && admin.profile_pic_path ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("img", {
      src: admin === null || admin === void 0 ? void 0 : admin.profile_pic_path,
      alt: "avatar",
      style: {
        display: "inline",
        height: "auto",
        width: "100%",
        margin: "auto 0"
      }
    }) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("img", {
      src: _public_assets_general_User_placeholder_svg__WEBPACK_IMPORTED_MODULE_15___default.a,
      alt: "avatar",
      style: {
        display: "inline",
        height: "auto",
        width: "100%",
        margin: "auto 0"
      }
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(Title, {
      style: {
        fontSize: "14px",
        marginTop: "0.5em",
        marginLeft: "5px",
        color: "white",
        textTransform: "capitalize"
      }
    }, admin === null || admin === void 0 ? void 0 : admin.first_name, " ", admin === null || admin === void 0 ? void 0 : admin.last_name))
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "account.profile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__["InertiaLink"], {
    href: route("account.profile")
  }, "Profile")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "account.changePwd"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__["InertiaLink"], {
    href: route("account.changePwd")
  }, "Change Password")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "account.logout"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__["InertiaLink"], {
    href: route("admin.logout")
  }, "Log Out")))))))))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    className: "app_header"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_row__WEBPACK_IMPORTED_MODULE_5___default.a, {
    type: "flex",
    justify: "center",
    align: "middle",
    className: "app_menu_wrapper"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default.a, {
    span: 22,
    className: "app_header_mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_14__["InertiaLink"], {
    href: "website",
    style: {
      color: "white"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("img", {
    src: _public_assets_general_webmob_logo_svg__WEBPACK_IMPORTED_MODULE_16___default.a,
    className: "app_logo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", {
    style: {
      display: "flex",
      "float": "right",
      flexDirection: "row"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_button__WEBPACK_IMPORTED_MODULE_3___default.a, {
    type: "link",
    onClick: showDrawer,
    style: {
      padding: "8px 8px",
      color: "white"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["MenuOutlined"], null))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_col__WEBPACK_IMPORTED_MODULE_7___default.a, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_drawer__WEBPACK_IMPORTED_MODULE_1___default.a, {
    placement: "left",
    closable: false,
    onClose: onClose,
    visible: visible,
    bodyStyle: {
      padding: 0
    },
    className: "drawer_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a, {
    defaultSelectedKeys: ["1"],
    mode: "inline",
    theme: "dark"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "1",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["PieChartOutlined"], null)
  }, "Option 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "2",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["DesktopOutlined"], null)
  }, "Option 2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(SubMenu, {
    key: "sub1",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["UserOutlined"], null),
    title: "User"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "3"
  }, "Tom"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "4"
  }, "Bill"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "5"
  }, "Alex")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(SubMenu, {
    key: "sub2",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["TeamOutlined"], null),
    title: "Team"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "6"
  }, "Team 1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "8"
  }, "Team 2")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(antd_lib_menu__WEBPACK_IMPORTED_MODULE_11___default.a.Item, {
    key: "9",
    icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12___default.a.createElement(_ant_design_icons__WEBPACK_IMPORTED_MODULE_13__["FileOutlined"], null)
  }, "Files")))))));
}

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./resources/js/Shared/AdminLayout/index.jsx":
/*!***************************************************!*\
  !*** ./resources/js/Shared/AdminLayout/index.jsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/lib/layout/style */ "./node_modules/antd/lib/layout/style/index.js");
/* harmony import */ var antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(antd_lib_layout_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/layout */ "./node_modules/antd/lib/layout/index.js");
/* harmony import */ var antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_layout__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Header */ "./resources/js/Shared/AdminLayout/Header.jsx");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./resources/js/Shared/AdminLayout/Footer.jsx");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _AdminLayout_AdminSider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../AdminLayout/AdminSider */ "./resources/js/Shared/AdminLayout/AdminSider.jsx");



function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var Header = antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a.Header,
    Content = antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a.Content,
    Footer = antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a.Footer;

function index(props) {
  var auth = Object(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_6__["usePage"])().props.auth;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false),
      _useState2 = _slicedToArray(_useState, 2),
      collapsed = _useState2[0],
      setCollapsed = _useState2[1];

  var handleCollapsed = function handleCollapsed(collapsed) {
    setCollapsed(collapsed);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a, {
    style: {
      minHeight: "100vh"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_5__["default"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("title", null, props.title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Header, {
    className: "bg_white",
    style: {
      zIndex: 5,
      width: "100%",
      padding: 0,
      position: "fixed",
      top: "0"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_Header__WEBPACK_IMPORTED_MODULE_3__["default"], {
    admin: auth === null || auth === void 0 ? void 0 : auth.admin,
    handleCollapsed: handleCollapsed
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a, {
    style: {
      paddingTop: '64px'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_AdminLayout_AdminSider__WEBPACK_IMPORTED_MODULE_7__["default"], {
    collapsed: collapsed,
    admin: auth === null || auth === void 0 ? void 0 : auth.admin
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(antd_lib_layout__WEBPACK_IMPORTED_MODULE_1___default.a, {
    className: "bg_app_content_color"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Content, {
    style: {
      margin: "0 16px",
      padding: "1em"
    }
  }, props.children), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Footer, {
    className: "footer app_footer"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], null)))));
}

/* harmony default export */ __webpack_exports__["default"] = (index);

/***/ })

}]);