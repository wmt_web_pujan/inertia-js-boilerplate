<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \Spatie\Permission\Models\Role::updateOrCreate(array(
            'name' => 'super admin',
            'guard_name' => 'admin'
        ));
        if ($role) {
            $role->givePermissionTo(\Spatie\Permission\Models\Permission::all());
        }
    }
}
