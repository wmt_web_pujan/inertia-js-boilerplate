import React, { useState } from "react";
import { Layout } from "antd";
import AdminHeader from "./Header";
import AdminFooter from "./Footer";
import Helmet from "react-helmet";
import { usePage } from "@inertiajs/inertia-react";
import AdminSider from "../AdminLayout/AdminSider";

const { Header, Content, Footer } = Layout;

function index(props) {
    const { auth } = usePage().props;
    const [collapsed, setCollapsed] = useState(false);

    const handleCollapsed = collapsed => {
        setCollapsed(collapsed);
    };

    return (
        <Layout style={{ minHeight: "100vh" }}>
            <Helmet>
                <title>{props.title}</title>
            </Helmet>

            <Header
                className="bg_white"
                style={{
                    zIndex: 5,
                    width: "100%",
                    padding: 0,
                    position: "fixed",
                    top: "0"
                }}
            >
                <AdminHeader
                    admin={auth?.admin}
                    handleCollapsed={handleCollapsed}
                />
            </Header>
            <Layout style={{paddingTop:'64px'}}>
                <AdminSider collapsed={collapsed} admin={auth?.admin}/>
                <Layout className="bg_app_content_color">
                    <Content style={{ margin: "0 16px", padding: "1em" }}>
                        {props.children}
                    </Content>
                    <Footer className="footer app_footer">
                        <AdminFooter />
                    </Footer>
                </Layout>
            </Layout>
        </Layout>
    );
}

export default index;
