<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";

    protected $fillable = [
        'first_name','last_name','email','profile_pic_path','email_verified_at','password','status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
