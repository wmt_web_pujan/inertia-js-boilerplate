<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Lab404\Impersonate\Models\Impersonate;

class Admin extends Authenticatable
{
    use HasRoles,Impersonate;
    protected $guard = 'admin';
    protected $table = "admins";

    protected $fillable = [
        'first_name','last_name','email','password','profile_pic_path','forget_password_code','status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getProfilePicPathAttribute()
    {
        if (isset($this->attributes['profile_pic_path']) && $this->attributes['profile_pic_path'] !== null && $this->attributes['profile_pic_path'] !== "") {
            return url('/storage/' . $this->attributes['profile_pic_path']);
        }
        return null;
    }

}
