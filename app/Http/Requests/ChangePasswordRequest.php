<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_pwd' => 'required',
            'new_pwd' => 'required|regex:/^(?=.*\d)(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$/',
            'confirm_pwd' => 'required|same:new_pwd'
        ];
    }
}
