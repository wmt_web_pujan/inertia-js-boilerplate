import React from "react";
import {Layout, Row, Col, Typography} from "antd";

const {Content} = Layout;
const {Title} = Typography;

function AuthLayout(props) {
    return (
        <Layout>
            <Content  className="bg_app_content_color">
                <Row>
                    <Col span={24} style={{minHeight: '100vh',display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                        <Title level={4} style={{textAlign: 'center'}}>{props.title}</Title>
                        {props.children}
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
}

export default AuthLayout;
