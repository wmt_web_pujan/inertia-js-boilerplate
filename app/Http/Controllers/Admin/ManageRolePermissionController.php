<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ManageRolePermissionController extends Controller
{
    public function role()
    {
        $role = Role::with('permissions')->get();
        $permission = Permission::all();
        return Inertia::render('Admin/ManageRole/Role', array(
            'role' => $role,
            'permission' => $permission,
        ));
    }

    public function addRole(Request $request)
    {
        $validated = Validator::make(
            $request->all(), [
            'role' => 'required',
            'permission' => 'required|array',
        ]);

        if ($validated->fails()) {
            return Redirect::back()->with(array('error' => $validated->messages()->first(), 'data' => uniqid()));
        }

        DB::beginTransaction();
        try {
            $role = Role::create(array(
                'name' => $request->role
            ));
            $role->givePermissionTo($request->permission);
            DB::commit();
            return Redirect::back()->with(array('success' => "Role Added successfully", 'data' => uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return Redirect::back()->with(array('error' => "Temporary server error. Try again.", 'data' => uniqid()));
        }
    }

    public function updateRolePermission(Request $request, $id)
    {
        $validated = Validator::make(
            $request->all(), [
            'permission' => 'required|array',
        ]);

        if ($validated->fails()) {
            return Redirect::back()->with(array('error' => $validated->messages()->first(), 'data' => uniqid()));
        }

        DB::beginTransaction();
        try {
            $role = Role::find((int)$id);
            $role->syncPermissions($request->permission);
            DB::commit();
            return Redirect::back()->with(array('success' => "Role Admin update successfully", 'data' => uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return Redirect::back()->with(array('error' => "Temporary server error. Try again.", 'data' => uniqid()));
        }
    }

    public function permission()
    {
        $role = Role::with('permissions')->get();
        $admin = Admin::where('id', '!=', auth()->id())->with('roles')->get();
        return Inertia::render('Admin/ManageRole/Admin', array(
            'role' => $role,
            'admin' => $admin
        ));
    }

    public function addAdmin(Request $request)
    {
        $validated = Validator::make(
            $request->all(), [
            'role' => 'required|array',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status' => 'required',
        ]);

        if ($validated->fails()) {
            return Redirect::back()->with(array('error' => $validated->messages()->first(), 'data' => uniqid()));
        }

        DB::beginTransaction();
        try {
            $admin = Admin::create(array(
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'status' => $request->status,
            ));

            $admin->assignRole($request->role);
            DB::commit();
            return Redirect::back()->with(array('success' => "Admin Added successfully", 'data' => uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return Redirect::back()->with(array('error' => "Temporary server error. Try again.", 'data' => uniqid()));
        }
    }

    public function editAdmin(Request $request, $id)
    {
        $validated = Validator::make(
            $request->all(), [
            'role' => 'required|array',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'status' => 'required',
        ]);

        if ($validated->fails()) {
            return Redirect::back()->with(array('error' => $validated->messages()->first(), 'data' => uniqid()));
        }

        DB::beginTransaction();
        try {
            $admin = Admin::where('id', (int)$id)->first();
            $admin->update(array(
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'status' => $request->status,
            ));

            $admin->syncRoles($request->role);
            DB::commit();
            return Redirect::back()->with(array('success' => "Admin Updated successfully", 'data' => uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return Redirect::back()->with(array('error' => "Temporary server error. Try again.", 'data' => uniqid()));
        }
    }

    public function impersonate($user_id)
    {
        $user = Admin::find($user_id);
        auth()->guard('admin')->user()->impersonate($user);
        return redirect()->route('dashboard');
    }
}
