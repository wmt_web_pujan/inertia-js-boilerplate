import React from "react";
import AdminLayout from "../../Shared/AdminLayout";
import { Row, Col, Card } from "antd";
import { UserOutlined, TeamOutlined } from "@ant-design/icons";

export default function dashBoard() {
    return (
        <AdminLayout title="Dash Board">
            <Row
                style={{ marginTop: "2em" }}
                gutter={[{ xl: 32, lg: 32, md: 0, sm: 0, xs: 0 }, 48]}
            >
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#003566" }}
                    >
                        <div className="card_logo">
                            <div>
                                <UserOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Active Users
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#004280" }}
                    >
                        <div className="card_logo">
                            <div>
                                <TeamOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Inactive Users
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#003566" }}
                    >
                        <div className="card_logo">
                            <div>
                                <UserOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Active Users
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#004280" }}
                    >
                        <div className="card_logo">
                            <div>
                                <TeamOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Active Users
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#003566" }}
                    >
                        <div className="card_logo">
                            <div>
                                <UserOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Active Users
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={24} md={24} lg={8} xl={8}>
                    <Card
                        hoverable
                        className="dashboard_card"
                        style={{ color: "white", backgroundColor: "#004280" }}
                    >
                        <div className="card_logo">
                            <div>
                                <TeamOutlined />
                            </div>
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "24px", fontWeight: "600" }}
                        >
                            18
                        </div>
                        <div
                            className="index_of_element"
                            style={{ fontSize: "18px" }}
                        >
                            Active Users
                        </div>
                    </Card>
                </Col>
            </Row>
        </AdminLayout>
    );
}
