import React, { useState, useRef, useEffect } from "react";
import { Button, Col, Form, Input, Modal, Row } from "antd";
import { Inertia } from "@inertiajs/inertia";

export default function ChangePasswordModal(props) {
    const { userId, closeModal } = props;
    const formRef = useRef(null);
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        setVisible(props?.visible);
    }, [props?.visible]);

    const handleOk = e => {
        closeModal(false);
    };

    const handleCancel = e => {
        formRef.current.resetFields();
        closeModal(false);
    };

    const onFinish = values => {
        const {userId} = props;
        closeModal(false);
        formRef.current.resetFields();
        Inertia.post(route("user.change-pwd",userId), values);
    };

    const onFinishFailed = values => {
        console.log(values, "error");
    };

    return (
        <div>
            <Modal
                title="Change Password"
                centered
                visible={visible}
                footer={null}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <Form
                    ref={formRef}
                    layout="vertical"
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                    hideRequiredMark
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row type="flex" justify="center" align="middle">
                        <Col span={24}>
                            <Row type="flex" justify="center" align="middle">
                                <Col span={24}>
                                    <label>Enter Current Password</label>
                                </Col>
                                <Col span={24}>
                                    <Form.Item
                                        name="current_pwd"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter current password."
                                            }
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter Current Password" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <label>Enter New Password</label>
                                </Col>
                                <Col span={24}>
                                    <Form.Item
                                        name="new_pwd"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter new password."
                                            },
                                            {
                                                pattern: /^(?=.*\d)(?=.*[0-9])(?=.*[a-zA-Z]).{8,}/,
                                                message:
                                                    "The password should be 8 characters atleast and should have atleast one number."
                                            }
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter New Password" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <label>Enter Password Again</label>
                                </Col>
                                <Col span={24}>
                                    <Form.Item
                                        name="confirm_pwd"
                                        style={{ marginBottom: "12px" }}
                                        dependencies={["new_pwd"]}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter password again."
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(rule, value) {
                                                    if (
                                                        !value ||
                                                        getFieldValue(
                                                            "new_pwd"
                                                        ) === value
                                                    ) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        "password and confirm password do not match."
                                                    );
                                                }
                                            })
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter Password Again" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <div
                                        style={{
                                            float: "right"
                                        }}
                                    >
                                        <Button
                                            type="link"
                                            style={{ margin: "0px 10px" }}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Update
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </div>
    );
}
