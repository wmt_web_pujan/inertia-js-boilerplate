import React, {useState, useEffect, useRef} from "react";
import {
    Table,
    Row,
    Col,
    Typography,
    Tag,
    Dropdown,
    Menu,
    notification,
    Breadcrumb,
    Modal,
    Button, Form, Input, Select
} from "antd";
import {MenuOutlined, HomeOutlined} from "@ant-design/icons";
import AdminLayout from "../../Shared/AdminLayout/index";
import {InertiaLink} from "@inertiajs/inertia-react";
import {Inertia} from "@inertiajs/inertia";
import User from "../../../../public/assets/general/User_placeholder.svg";
import ChangePasswordModal from "../../Components/ChangePasswordModal";

const {Title} = Typography;
const {Option} = Select;

export default function user(props) {
    const {auth: {admin}} = props;
    const formRef = useRef(null);

    const [role, setRole] = useState(admin?.role?.find(e => e.name === "super admin"))
    const [permission, setPermission] = useState(admin?.role.find(e => e.name === "user")?.permissions?.map(e => e.name));
    const [modalVisible, setModalVisible] = useState(false);
    const [userModalVisible, setUserModalVisible] = useState(false);
    const [userId, setUserId] = useState(null);
    const [permissionModal, setPermissionModal] = useState(false);
    const [isActive, setIsActive] = useState(null);

    const handleUserModal = () => {
        setUserModalVisible(true);
    }

    const closeModal = value => {
        setModalVisible(value);
    };

    const onChange = (pagination, filters) => {
        if (filters?.status) {
            Inertia.replace(route("user", {status: filters?.status}));
        }
    };

    const onFinish = (values) => {
        console.log(values, "success");
    }

    const onFinishFailed = (values) => {
        console.log(values, "error")
    }

    const handleClick = ({key}, userId, userData) => {
        switch (key) {
            case "inactive":
                setPermissionModal(true);
                setIsActive(userData?.status);
                setUserId(userId);
                break;
            case "active":
                setPermissionModal(true);
                setIsActive(userData?.status);
                setUserId(userId);
                break;
            case "change_pwd":
                setModalVisible(true);
                setUserId(userId);
                break;
        }
    };

    const handleOk = () => {
        setPermissionModal(false);
    };

    const handleCancel = () => {
        formRef.current.resetFields();
        setPermissionModal(false);
    };

    const handleUserModalOk = () => {
        formRef.current.resetFields();
        setUserModalVisible(false);
    }

    const handleUserModalCancel = () => {
        formRef.current.resetFields();
        setUserModalVisible(false);
    };

    const handleUserIsActive = () => {
        if (isActive && isActive === 1) {
            Inertia.post(route("user.inactive", userId));
            setPermissionModal(false);
        } else {
            Inertia.post(route("user.active", userId));
            setPermissionModal(false);
        }
    };

    useEffect(() => {
        props.flash.error &&
        notification.error({
            message: props.flash.error
        });

        props.flash.success &&
        notification.success({
            message: props.flash.success
        });
    }, [props?.flash?.data]);

    const usersData = [];

    props?.users &&
    props?.users.length > 0 &&
    props?.users?.map((e, index) =>
        usersData.push({
            key: index,
            profile_pic: (
                <>
                    {e.profile_pic_path ? (
                        <img
                            src={e.profile_pic_path}
                            alt="avatar"
                            height="40px"
                            width="40px"
                            style={{borderRadius: "50%"}}
                        />
                    ) : (
                        <img
                            src={User}
                            alt="avatar"
                            height="40px"
                            width="40px"
                            style={{borderRadius: "50%"}}
                        />
                    )}
                </>
            ),
            first_name: e.first_name,
            last_name: e.last_name,
            email: e.email,
            status: e.status,
            action: e
        })
    );

    const columns = [
        {
            dataIndex: "profile_pic",
            key: "profile_pic",
            width: "5%"
        },
        {
            title: "First Name",
            dataIndex: "first_name",
            key: "first_name",
            sorter: {
                compare: (a, b) => a.first_name.length - b.first_name.length
            }
        },
        {
            title: "Last Name",
            dataIndex: "last_name",
            key: "last_name",
            sorter: (a, b) => a.last_name.length - b.last_name.length
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "email",
            sorter: (a, b) => a.email.length - b.email.length
        },
        {
            title: "Status",
            dataIndex: "status",
            key: "status",
            filters: [
                {
                    text: "Active",
                    value: 1
                },
                {
                    text: "Inactive",
                    value: 0
                }
            ],
            render: status => (
                <>
                    {status === 1 ? (
                        <Tag color="green">Active</Tag>
                    ) : (
                        <Tag color="red">Inactive</Tag>
                    )}
                </>
            )
        }
    ];

    /*check edit permission*/
    (role?.permissions?.map(e => e.name).includes("edit") || permission?.includes("edit")) && columns.push({
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: action => (
            <Dropdown
                overlay={
                    <>
                        <Menu
                            onClick={key =>
                                handleClick(key, action.id, action)
                            }
                            mode="inline"
                        >
                            {action.status === 1 ? (
                                <Menu.Item
                                    key="inactive"
                                    style={{margin: "0px"}}
                                >
                                    Inactive
                                </Menu.Item>
                            ) : (
                                <Menu.Item
                                    key="active"
                                    style={{margin: "0px"}}
                                >
                                    Active
                                </Menu.Item>
                            )}
                            <Menu.Item key="view_detail">
                                View Profile
                            </Menu.Item>
                            <Menu.Item key="change_pwd">
                                Change Password
                            </Menu.Item>
                        </Menu>
                    </>
                }
            >
                <MenuOutlined/>
            </Dropdown>
        )
    })

    return (
        <AdminLayout title="User">
            <Row style={{marginTop: "2.0em"}}>
                <Col span={24}>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <InertiaLink href={route("dashboard")}>
                                <HomeOutlined/>
                            </InertiaLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <span className="text_app_primary_color">
                                Users
                            </span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </Col>
            </Row>
            <Row style={{marginTop: "10px"}}>
                {
                    (role?.permissions?.map(e => e.name).includes("add") || permission?.includes("add")) ?
                        <Col span={24} style={{marginTop: "1.0em"}}>
                            <Button type="primary" style={{float: 'right'}} onClick={handleUserModal}>
                                Add User
                            </Button>
                        </Col> : null
                }

                <Col span={24} style={{marginTop: "1.0em"}}>
                    <Table
                        onChange={onChange}
                        scroll={{x: true}}
                        dataSource={usersData}
                        columns={columns}
                    />
                </Col>
            </Row>

            <ChangePasswordModal
                visible={modalVisible}
                userId={userId}
                closeModal={closeModal}
            />

            <Modal
                title="User Admin"
                visible={permissionModal}
                onOk={handleOk}
                footer={null}
                centered
                onCancel={handleCancel}
            >
                <Row>
                    <Col span={24}>
                        <p>
                            Are you sure do you want to{" "}
                            {isActive === 1 ? "inactive" : "active"} user?
                        </p>
                    </Col>
                    <Col span={24}>
                        <div
                            style={{
                                float: "right"
                            }}
                        >
                            <Button
                                type="link"
                                style={{margin: "0px 10px"}}
                                onClick={() => setPermissionModal(false)}
                            >
                                Cancel
                            </Button>
                            <Button type="primary" onClick={handleUserIsActive}>
                                Yes
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Modal>


            <Modal title="Add User" visible={userModalVisible} onOk={handleUserModalOk} onCancel={handleUserModalCancel}
                   centered
                   footer={null}>
                <Form
                    ref={formRef}
                    layout="vertical"
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                    size="large"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row>
                        <Col span={24}>
                            <label>First Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="first_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter first name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter First Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Last Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="last_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter last name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Last Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Email</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="email"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter email."
                                    }, {
                                        type: "email",
                                        message: "Please enter valid email format."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Email"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Password</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="password"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter password."
                                    },
                                    {
                                        pattern: /^(?=.*\d)(?=.*[0-9])(?=.*[a-zA-Z]).{8,}/,
                                        message:
                                            "The password should be 8 characters at least and should have at least one number."
                                    }
                                ]}
                            >
                                <Input.Password placeholder="Enter Password"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>User Status</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select user status"
                                    }
                                ]}
                            >
                                <Select
                                    placeholder="Select a user status"
                                    showSearch
                                >
                                    <Option value={1}>Active</Option>
                                    <Option value={0}>Inactive</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <div
                                style={{
                                    float: "right"
                                }}
                            >
                                <Button
                                    type="link"
                                    style={{margin: "0px 10px"}}
                                    size="middle"
                                    onClick={handleUserModalCancel}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="middle"
                                >
                                    Submit
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </AdminLayout>
    );
}
