const mix = require("laravel-mix");
const path = require("path");
const fs = require("fs");
const lessToJs = require("less-vars-to-js");

const themevars = lessToJs(
    fs.readFileSync(path.join(__dirname, "resources/less/theme.less"), "utf-8")
);

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath("public").react("resources/js/app.js", "public/js")
    .styles(["resources/css/ngprogress.css"], "public/css/app.css")
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.less$/,
                    use: [
                        {
                            loader: "less-loader",
                            options: {
                                modifyVars: themevars,
                                javascriptEnabled: true
                            }
                        }
                    ],
                    exclude: [path.resolve(__dirname, "node-modules")]
                }
            ]
        }
    })
    .webpackConfig({
        output: { chunkFilename: "js/[name].js?id=[chunkhash]" },
        resolve: {
            alias: {
                "@": path.resolve("resources/js")
            }
        }
    })
    .babelConfig({
        plugins: [
            "@babel/plugin-syntax-dynamic-import",
            ["import", { libraryName: "antd", style: true }]
        ]
    })
    .options({
        terser: {
            cache: true,
            parallel: 5
        },
        uglify: {
            uglifyOptions: {
                mangle: true,
                unused: true,
                dead_code: true, // big one--strip code that will never execute
                warnings: false, // good for prod apps so users can't peek behind curtain
                drop_debugger: true,
                conditionals: true,
                evaluate: true,
                drop_console: true, // strips console statements
                sequences: true,
                booleans: true,
                compress: {
                    warnings: false, // Suppress uglification warnings
                    pure_getters: true,
                    unsafe: true,
                    unsafe_comps: true,
                    screw_ie8: true
                },
                output: {
                    comments: false
                }
            }
        }
    })
    .version();
