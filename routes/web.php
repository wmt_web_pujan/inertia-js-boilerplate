<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Admin')->group(function () {
    Route::namespace('Auth')->group(function () {
        Route::get('/', 'AuthController@loginForm')->name('login');
        Route::post('/admin-login', 'AuthController@login')->name('admin.login');
        Route::get('/admin-logout', 'AuthController@logout')->name('admin.logout');
    });

    Route::middleware('auth:admin')->group(function () {
        Route::get('/dashboard', 'DashBoardController@dashBoardPage')->name('dashboard')->middleware('role:super admin|dashboard');

        //role & permission
        Route::middleware('role:super admin')->group(function () {

            //impersonate
            Route::get('/impersonate/{user_id}','ManageRolePermissionController@impersonate')->name('impersonate');

            Route::get('/role', 'ManageRolePermissionController@role')->name('acl.role');
            Route::post('/add-role', 'ManageRolePermissionController@addRole')->name('add.role');
            Route::post('/update/permission/{id}', 'ManageRolePermissionController@updateRolePermission')->name('update.role.permission');
            Route::get('/admin', 'ManageRolePermissionController@permission')->name('acl.admin');
            Route::post('/add-admin', 'ManageRolePermissionController@addAdmin')->name('add.admin');
            Route::post('/edit-admin/{id}', 'ManageRolePermissionController@editAdmin')->name('edit.admin');
        });


        Route::middleware('role:super admin|user')->group(function () {
            //user Controller
            Route::get('/user', 'UserController@user')->name('user');
            Route::post('/user-active/{userId}', 'UserController@activeUser')->name('user.active');
            Route::post('/user-inactive/{userId}', 'UserController@inactiveUser')->name('user.inactive');
            Route::post('/user-change-password/{userId}', 'UserController@changePassword')->name('user.change-pwd');
        });


        //Account
        Route::get('/profile', 'AccountController@profile')->name('account.profile');
        Route::post('/update-profile', 'AccountController@updateProfile')->name('update-admin-profile');
        Route::get('/update-password', 'AccountController@updatePasswordPage')->name('account.changePwd');
        Route::post('/update-password', 'AccountController@changePassword')->name('change-admin-pwd');
    });
});
