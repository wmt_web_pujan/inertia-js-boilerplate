import React, {useEffect, useRef, useState} from "react";
import AdminLayout from "../../../Shared/AdminLayout/index";
import {Breadcrumb, Button, Col, Row, Table, Modal, Form, Input, Select, notification, Checkbox} from "antd";
import {InertiaLink} from "@inertiajs/inertia-react";
import {Inertia} from "@inertiajs/inertia";
import {HomeOutlined, EditOutlined} from "@ant-design/icons";

const {Option} = Select;

export default function Role(props) {
    const {role} = props;
    const formRef = useRef(null);
    const formEditRef = useRef(null);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [editRoleModal, setEditRoleModal] = useState(false);
    const [roleData, setRoleDate] = useState(null);
    const [permission, setPermission] = useState(props.permission);
    const [checkbox, setCheckbox] = useState([]);

    useEffect(() => {
        setCheckbox(role?.map((e, index) => e.permissions && e.permissions.length > 0 && e.permissions.map(x => x.name)));
    }, [])

    const handleCheckbox = (value, index) => {
        checkbox[index] = value;
        setCheckbox(checkbox);
    }

    const columns = [
        {
            title: "Role",
            dataIndex: "role",
            key: "role",
            render: (name) => (
                <>
                    <div style={{textTransform: 'capitalize'}}>{name}</div>
                </>
            )
        },
        {
            title: "Admin",
            dataIndex: "permission",
            key: "permission",
            render: (per, data, index) => (
                <>
                    <Checkbox.Group defaultValue={per?.map(e => e.name)} onChange={(e) => handleCheckbox(e, index)}>
                        {permission &&
                        permission.map(e =>
                            <Checkbox
                                value={e.name}>{e.name}</Checkbox>
                        )
                        }
                    </Checkbox.Group>
                </>
            )
        },
        {
            title: "Action",
            dataIndex: "action",
            key: "action",
            render: (e, data, index) => (
                <>
                    <Button type="primary"
                            onClick={() => Inertia.post(route('update.role.permission', e.id), {permission: checkbox[index]})}>Save</Button>
                </>
            )
        }
    ];

    const dataSource = [];

    role && role.length > 0 && role.map(e => dataSource.push({
        'role': e.name,
        'permission': e.permissions,
        'action': e,
    }));

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
        setEditRoleModal(false);
    };

    const handleCancel = () => {
        formRef.current.resetFields();
        setIsModalVisible(false);
        setEditRoleModal(false);
    };

    const onFinish = (values) => {
        Inertia.post(route('add.role', values));
        handleCancel()
    }

    const onFinishFailed = (values) => {
        console.log(values, "error");
    }

    useEffect(() => {
        props.flash.error &&
        notification.error({
            message: props.flash.error
        });

        props.flash.success &&
        notification.success({
            message: props.flash.success
        });
    }, [props?.flash?.data]);

    return (
        <AdminLayout title="Role">
            <Row style={{marginTop: "2.0em"}}>
                <Col span={24}>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <InertiaLink href={route("dashboard")}>
                                <HomeOutlined/>
                            </InertiaLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <span className="text_app_primary_color">
                                Role
                            </span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </Col>
            </Row>
            <Row style={{marginTop: "10px"}}>
                <Col span={24} style={{marginTop: "1.0em"}}>
                    <Button type="primary" style={{float: 'right'}} onClick={showModal}>
                        Add Role
                    </Button>
                </Col>
                <Col span={24} style={{marginTop: "1.0em"}}>
                    <Table
                        scroll={{x: true}}
                        dataSource={dataSource}
                        columns={columns}
                    />
                </Col>
            </Row>

            <Modal title="Add Role" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} centered
                   footer={null}>
                <Form
                    ref={formRef}
                    layout="vertical"
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                    size="large"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row>
                        <Col span={24}>
                            <Form.Item
                                name="role"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter role."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Role"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="permission"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select permission"
                                    }
                                ]}
                            >
                                <Select
                                    mode="multiple"
                                    placeholder="Select a permission"
                                    showSearch
                                >
                                    {
                                        permission.map(e => <Option value={e.name}>{e.name}</Option>)
                                    }
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <div
                                style={{
                                    float: "right"
                                }}
                            >
                                <Button
                                    type="link"
                                    style={{margin: "0px 10px"}}
                                    size="middle"
                                    onClick={handleCancel}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="middle"
                                >
                                    Submit
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </AdminLayout>
    );
}
