import React, { useState, useEffect } from "react";
import AdminLayout from "../../../Shared/AdminLayout/index";
import {
    Typography,
    Row,
    Col,
    Form,
    notification,
    Input,
    Button,
    Upload,
    Breadcrumb,
    Card
} from "antd";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { HomeOutlined } from "@ant-design/icons";
import User from "../../../../../public/assets/general/User_placeholder.svg";

const { Title } = Typography;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
}

export default function Profile(props) {
    const { profile } = props;
    const [imageUrl, setImageUrl] = useState(profile?.profile_pic_path);
    const [loading, setLoading] = useState(false);

    const dummyRequest = ({ onSuccess }) => {
        setTimeout(() => {
            onSuccess("ok");
        }, 0);
    };

    const beforeUpload = file => {
        const isJpgOrPng =
            file.type === "image/jpeg" || file.type === "image/png";
        if (!isJpgOrPng) {
            notification.error({
                placement: "bottomRight",
                message: "You can only upload JPG/PNG file."
            });
        }

        const isLt5M = file.size / 1024 / 1024 < 5;
        if (!isLt5M) {
            notification.error({
                placement: "bottomRight",
                message: "File size must smaller than 5MB."
            });
        }
        return isJpgOrPng && isLt5M;
    };

    const handleChange = info => {
        if (info.file.status === "uploading") {
            setLoading(true);
            return;
        }
        if (info.file.status === "done") {
            getBase64(info.file.originFileObj, image => {
                setLoading(false);
                setImageUrl(image);
            });
        }
    };

    const onFinish = values => {
        let data = new FormData();
        data.append("first_name", values.first_name || "");
        data.append("last_name", values.last_name || "");
        data.append("email", values.email || "");
        data.append(
            "profile_pic_path",
            (values.profile_pic_path &&
                values.profile_pic_path.file.originFileObj) ||
                ""
        );
        Inertia.post(route("update-admin-profile"), data);
    };

    const onFinishFailed = values => {
        console.log(values, "error");
    };

    useEffect(() => {
        props.flash.error &&
            notification.error({
                message: props.flash.error
            });

        props.flash.success &&
            notification.success({
                message: props.flash.success
            });
    }, [props?.flash?.data]);

    return (
        <AdminLayout title="Profile">
            <Row style={{ marginTop: "2.0em" }}>
                <Col span={24}>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <InertiaLink href={route("dashboard")}>
                                <HomeOutlined />
                            </InertiaLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <span className="text_app_primary_color">
                                Profile
                            </span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </Col>
            </Row>
            <Row type="flex" justify="center" align="middle">
                <Col span={24}>
                    <Form
                        layout="vertical"
                        name="basic"
                        initialValues={{
                            first_name: profile?.first_name,
                            last_name: profile?.last_name,
                            email: profile?.email
                        }}
                        size="large"
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                    >
                        <Row
                            type="flex"
                            justify="center"
                            align="middle"
                            gutter={[
                                { xs: 0, sm: 0, md: 32, lg: 32, xl: 32 },
                                0
                            ]}
                            style={{marginTop:'1.5em'}}
                        >
                            <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                                <Card>
                                    <div
                                        style={{
                                            display: "flex",
                                            flexDirection: "column",
                                            justifyContent: "center",
                                            alignItems: "center",
                                            marginTop: "10px"
                                        }}
                                    >
                                        {imageUrl ? (
                                            <div
                                                style={{
                                                    position: "relative",
                                                    width: "140px",
                                                    height: "140px",
                                                    overflow: "hidden",
                                                    borderRadius: "50%",
                                                    display: "flex",
                                                    padding: "5px",
                                                    border: "2px solid #DFEBF9"
                                                }}
                                            >
                                                <img
                                                    src={imageUrl}
                                                    alt="avatar"
                                                    style={{
                                                        display: "inline",
                                                        height: "auto",
                                                        width: "100%",
                                                        margin: "auto 0"
                                                    }}
                                                />
                                            </div>
                                        ) : (
                                            <div
                                                style={{
                                                    height: 140,
                                                    width: 140,
                                                    borderRadius: 100,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    display: "flex"
                                                }}
                                            >
                                                <span
                                                    style={{
                                                        borderRadius: 100,
                                                        fontWeight: "bold",
                                                        fontSize: 45
                                                    }}
                                                >
                                                    <img
                                                        src={User}
                                                        alt="avatar"
                                                        style={{
                                                            height: "140px",
                                                            width: "140px",
                                                            borderRadius: "50%"
                                                        }}
                                                    />
                                                </span>
                                            </div>
                                        )}
                                        <Form.Item name="profile_pic_path">
                                            <Upload
                                                name="avatar"
                                                showUploadList={false}
                                                customRequest={dummyRequest}
                                                beforeUpload={beforeUpload}
                                                onChange={handleChange}
                                                accept="image/*"
                                            >
                                                <Button
                                                    type="primary"
                                                    size="middle"
                                                    style={{ marginTop: "1em" }}
                                                >
                                                    Change Photo
                                                </Button>
                                            </Upload>
                                        </Form.Item>
                                    </div>
                                    <div>
                                        <label>Email</label>
                                    </div>
                                    <div>
                                        <Form.Item
                                            name="email"
                                            style={{ marginBottom: "12px" }}
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        "Please enter your email."
                                                },
                                                {
                                                    type: "email",
                                                    message:
                                                        "Email format is invalid."
                                                }
                                            ]}
                                        >
                                            <Input
                                                placeholder="Email"
                                                disabled={true}
                                            />
                                        </Form.Item>
                                    </div>
                                    <div>
                                        <label>First Name</label>
                                    </div>
                                    <div>
                                        <Form.Item
                                            name="first_name"
                                            style={{ marginBottom: "12px" }}
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        " Please type your first name."
                                                },
                                                {
                                                    whitespace: true,
                                                    message:
                                                        "only Whitespace are not allowed."
                                                }
                                            ]}
                                        >
                                            <Input
                                                placeholder="First Name"
                                                style={{
                                                    textTransform: "capitalize"
                                                }}
                                            />
                                        </Form.Item>
                                    </div>
                                    <div style={{ marginTop: "1.0em" }}>
                                        <label>Last Name</label>
                                    </div>
                                    <div>
                                        <Form.Item
                                            name="last_name"
                                            style={{ marginBottom: "12px" }}
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        "Please type your last name."
                                                },
                                                {
                                                    whitespace: true,
                                                    message:
                                                        "only Whitespace are not allowed."
                                                }
                                            ]}
                                        >
                                            <Input
                                                placeholder="Last Name"
                                                style={{
                                                    textTransform: "capitalize"
                                                }}
                                            />
                                        </Form.Item>
                                    </div>
                                    <div
                                        style={{
                                            float: "right"
                                        }}
                                    >
                                        <Button
                                            type="link"
                                            size="middle"
                                            style={{ margin: "0px 10px" }}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            size="middle"
                                        >
                                            Update
                                        </Button>
                                    </div>
                                </Card>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </AdminLayout>
    );
}
