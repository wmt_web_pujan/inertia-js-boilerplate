import React, {useEffect, useRef, useState} from "react";
import AdminLayout from "../../../Shared/AdminLayout/index";
import {Button, Col, Dropdown, Form, Input, Menu, Modal, notification, Row, Select, Table, Tag} from "antd";
import {Inertia} from "@inertiajs/inertia";
import User from "../../../../../public/assets/general/User_placeholder.svg";
import {MenuOutlined} from "@ant-design/icons";
import {InertiaLink} from "@inertiajs/inertia-react";

const {Option} = Select;

export default function Admin(props) {
    const {role, admin} = props;
    const formRef = useRef(null);
    const formEditRef = useRef(null);
    const [isVisible, setIsVisible] = useState(false);
    const [isEditModalVisible, setIsEditModalVisible] = useState(false);
    const [adminUser, setAdminUser] = useState(null);
    const [adminId, setAdminId] = useState(null);

    const handleModalOk = () => {
        formRef.current.resetFields();
        setIsVisible(false);
    }

    const handleModalCancel = () => {
        formRef.current.resetFields();
        setIsVisible(false);
    }

    const handleEditModalOk = () => {
        formEditRef.current.resetFields();
        setIsEditModalVisible(false);
    }

    const handleEditModalCancel = () => {
        formEditRef.current.resetFields();
        setIsEditModalVisible(false);
    }

    const onFinish = (values) => {
        Inertia.post(route('add.admin', values));
        setIsVisible(false);
        formRef.current.resetFields();
    }

    const onFinishFailed = (values) => {
        console.log(values, "error")
    }

    const handleMenuClick = ({key}, action, id) => {
        switch (key) {
            case "edit":
                setIsEditModalVisible(true);
                setAdminUser(action);
                setAdminId(id);
                formEditRef?.current?.setFieldsValue({
                    first_name: action?.first_name,
                    last_name: action?.last_name,
                    email: action?.email,
                    status: action?.status,
                    role: action?.roles?.map(e => e.name)
                });
                break;
        }
    }

    const submitEditAdmin = (values) => {
        Inertia.post(route('edit.admin', adminId), values);
        setIsEditModalVisible(false);
        formEditRef.current.resetFields();
    }

    useEffect(() => {
        props.flash.error &&
        notification.error({
            message: props.flash.error
        });

        props.flash.success &&
        notification.success({
            message: props.flash.success
        });
    }, [props?.flash?.data]);

    const columns = [
        {
            dataIndex: "profile_pic",
            key: "profile_pic",
            width: "5%"
        },
        {
            title: "First Name",
            dataIndex: "first_name",
            key: "first_name",
            sorter: {
                compare: (a, b) => a.first_name.length - b.first_name.length
            }
        },
        {
            title: "Last Name",
            dataIndex: "last_name",
            key: "last_name",
            sorter: (a, b) => a.last_name.length - b.last_name.length
        },
        {
            title: "Email",
            dataIndex: "email",
            key: "email",
            sorter: (a, b) => a.email.length - b.email.length
        },
        {
            title: "Role",
            dataIndex: "role",
            key: "role",
        },
        {
            title: "Status",
            dataIndex: "status",
            key: "status",
            render: status => (
                <>
                    {status === 1 ? (
                        <Tag color="green">Active</Tag>
                    ) : (
                        <Tag color="red">Inactive</Tag>
                    )}
                </>
            )
        },
        {
            title: "Action",
            dataIndex: "action",
            key: "action",
            render: action => (
                <Dropdown
                    overlay={
                        <>
                            <Menu
                                onClick={key =>
                                    handleMenuClick(key, action, action?.id)
                                }
                                mode="inline"
                            >
                                <Menu.Item
                                    key="edit"
                                    style={{margin: "0px"}}
                                >
                                    Edit Admin
                                </Menu.Item>
                                <Menu.Item
                                    key="login"
                                    style={{margin: "0px"}}
                                >
                                    <InertiaLink href={route('impersonate', action?.id)}>
                                        Login
                                    </InertiaLink>
                                </Menu.Item>
                            </Menu>
                        </>
                    }
                >
                    <MenuOutlined/>
                </Dropdown>
            )
        }
    ];

    const adminData = [];

    admin && admin.length > 0 && admin.map((e, index) => adminData.push({
        key: index,
        profile_pic: (
            <>
                {e.profile_pic_path ? (
                    <img
                        src={e.profile_pic_path}
                        alt="avatar"
                        height="40px"
                        width="40px"
                        style={{borderRadius: "50%"}}
                    />
                ) : (
                    <img
                        src={User}
                        alt="avatar"
                        height="40px"
                        width="40px"
                        style={{borderRadius: "50%"}}
                    />
                )}
            </>
        ),
        first_name: e.first_name,
        last_name: e.last_name,
        email: e.email,
        role: e.roles?.map((e, index, array) => `${e.name}${(index !== (array.length - 1) ? ', ' : '')}`),
        status: e.status,
        action: e,
    }))

    return (
        <AdminLayout title="Admin">
            <Row style={{marginTop: "10px"}}>
                <Col span={24} style={{marginTop: "1.0em"}}>
                    <Button type="primary" style={{float: 'right'}} onClick={() => setIsVisible(true)}>
                        Add Admin
                    </Button>
                </Col>

                <Col span={24} style={{marginTop: "1.0em"}}>
                    <Table
                        scroll={{x: true}}
                        dataSource={adminData}
                        columns={columns}
                    />
                </Col>
            </Row>

            <Modal title="Add Admin" visible={isVisible} onOk={handleModalOk} onCancel={handleModalCancel}
                   centered
                   footer={null}>
                <Form
                    ref={formRef}
                    layout="vertical"
                    name="basic"
                    initialValues={{
                        remember: true
                    }}
                    size="large"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row>
                        <Col span={24}>
                            <label>Admin Role</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="role"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select admin role."
                                    }
                                ]}
                            >
                                <Select
                                    mode="multiple"
                                    placeholder="Select a admin role"
                                    showSearch
                                >
                                    {
                                        role?.map(e =>
                                            <Option value={e.name}>{e.name}</Option>
                                        )
                                    }
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>First Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="first_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter first name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter First Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Last Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="last_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter last name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Last Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Email</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="email"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter email."
                                    }, {
                                        type: "email",
                                        message: "Please enter valid email format."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Email"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Password</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="password"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter password."
                                    },
                                    {
                                        pattern: /^(?=.*\d)(?=.*[0-9])(?=.*[a-zA-Z]).{8,}/,
                                        message:
                                            "The password should be 8 characters at least and should have at least one number."
                                    }
                                ]}
                            >
                                <Input.Password placeholder="Enter Password"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Status</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select user status"
                                    }
                                ]}
                            >
                                <Select
                                    placeholder="Select a user status"
                                    showSearch
                                >
                                    <Option value={1}>Active</Option>
                                    <Option value={0}>Inactive</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <div
                                style={{
                                    float: "right"
                                }}
                            >
                                <Button
                                    type="link"
                                    style={{margin: "0px 10px"}}
                                    size="middle"
                                    onClick={handleModalCancel}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="middle"
                                >
                                    Submit
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Modal>


            <Modal title="Edit Admin" visible={isEditModalVisible} onOk={handleEditModalOk}
                   onCancel={handleEditModalCancel}
                   centered
                   footer={null}>
                <Form
                    ref={formEditRef}
                    layout="vertical"
                    name="basic"
                    initialValues={{
                        first_name: adminUser?.first_name,
                        last_name: adminUser?.last_name,
                        email: adminUser?.email,
                        status: adminUser?.status,
                        role: adminUser?.roles?.map(e => e.name)
                    }}
                    size="large"
                    onFinish={submitEditAdmin}
                    onFinishFailed={onFinishFailed}
                >
                    <Row>
                        <Col span={24}>
                            <label>Admin Role</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="role"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select admin role."
                                    }
                                ]}
                            >
                                <Select
                                    mode="multiple"
                                    placeholder="Select a admin role"
                                    showSearch
                                >
                                    {
                                        role?.map(e =>
                                            <Option value={e.name}>{e.name}</Option>
                                        )
                                    }
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>First Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="first_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter first name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter First Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Last Name</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="last_name"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter last name."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Last Name"/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Email</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="email"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter email."
                                    }, {
                                        type: "email",
                                        message: "Please enter valid email format."
                                    }
                                ]}
                            >
                                <Input placeholder="Enter Email" disabled={true}/>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <label>Status</label>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                style={{marginBottom: "12px"}}
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please select user status"
                                    }
                                ]}
                            >
                                <Select
                                    placeholder="Select a user status"
                                    showSearch
                                >
                                    <Option value={1}>Active</Option>
                                    <Option value={0}>Inactive</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <div
                                style={{
                                    float: "right"
                                }}
                            >
                                <Button
                                    type="link"
                                    style={{margin: "0px 10px"}}
                                    size="middle"
                                    onClick={handleModalCancel}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="middle"
                                >
                                    Submit
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </AdminLayout>
    );
}
