<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::updateOrCreate(array('name' => 'viewer', 'guard_name' => 'admin'));
        Permission::updateOrCreate(array('name' => 'add', 'guard_name' => 'admin'));
        Permission::updateOrCreate(array('name' => 'edit', 'guard_name' => 'admin'));
    }
}
