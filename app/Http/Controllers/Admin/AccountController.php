<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Model\Admin;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class AccountController extends Controller
{
    public function profile()
    {
        $adminId = auth()->id();
        $profile = Admin::where('id',$adminId)->first();
        return Inertia::render('Admin/Account/profile',array(
            'profile' => $profile
        ));
    }

    public function updateProfile(ProfileRequest $request){
        // will return only validate data
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Update Profile
            $adminId = auth()->id();

            $profilePicPath = null;
            if(isset($request->profile_pic_path)){
                $adminRandomId = auth()->id() . '_' . mt_rand(1111, 9999);
                $profilePic = $request->file('profile_pic_path');
                $ext = $profilePic->getClientOriginalExtension();
                $profilePicPath = $profilePic->storeAs('admin_profile_pic', $adminRandomId . '.' . $ext, 'public');

                Admin::where('id',$adminId)->update(array(
                    'profile_pic_path' => $profilePicPath,
                ));
            }

            Admin::where('id',$adminId)->update(array(
               'first_name' => $request->first_name,
               'last_name' => $request->last_name,
            ));
            DB::commit();
            return Redirect::route('account.profile')->with(array('success'=>"Profile Update successfully",'data'=>uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return Redirect::back()->with(array('error'=>"Temporary server error. Try again.",'data'=>uniqid()));
        }

    }

    public function updatePasswordPage(){
        return Inertia::render('Admin/Account/changePassword');
    }

    public function changePassword(ChangePasswordRequest $request){
        // will return only validate data
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            // Update Password
            $userId = auth()->id();
            $user = Admin::find($userId);
            if (!Hash::check($request->current_pwd, $user->password)) {
                return Redirect::back()->with(array('error'=>"Password is not match",'data'=>uniqid()));
            } else {
                $user->update([
                    'password' => Hash::make($request->new_pwd)
                ]);
                DB::commit();
                return Redirect::back()->with(array('success'=>"Password change successfully",'data'=>uniqid()));
            }
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()
                ->back()
                ->with(array('error'=>"Temporary server error. Try again.",'data'=>uniqid()));
        }
    }
}
