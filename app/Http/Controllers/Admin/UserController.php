<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\QueryException;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function user(Request $request){
        $users = User::select('*');
        if(isset($request->status)){
            $status = array_map(function($value) {
                return (int)$value;
            },$request->status);
            $users->whereIn('status',$status);
        }
        $users = $users->get();
        return Inertia::render('Admin/user',array(
            'users' => $users
        ));
    }

    public function inactiveUser($userId){
        DB::beginTransaction();
        try {
            User::where('id',(int)$userId)->update([
                'status' => 0
            ]);
            DB::commit();
            return Redirect::back()->with(array('success'=>"User Inactive successfully",'data'=>uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()
                ->back()
                ->with(array('error'=>"Temporary server error. Try again.",'data'=>uniqid()));
        }
    }

    public function activeUser($userId){
        DB::beginTransaction();
        try {
            User::where('id',(int)$userId)->update([
                'status' => 1
            ]);
            DB::commit();
         return Redirect::back()->with(array('success'=>"User Active successfully",'data'=>uniqid()));
        } catch (QueryException $e) {
            DB::rollBack();
            return redirect()
                ->back()
                ->with(array('error'=>"Temporary server error. Try again.",'data'=>uniqid()));
        }
    }

    public function changePassword(ChangePasswordRequest $request,$userId){
         // will return only validate data
         $validated = $request->validated();

         DB::beginTransaction();
         try {
             // Update Password
             $user = User::find((int)$userId);
             if (!Hash::check($request->current_pwd, $user->password)) {
                 return Redirect::back()->with(array('error'=>"Password is not match",'data'=>uniqid()));
             } else {
                 $user->update([
                     'password' => Hash::make($request->new_pwd)
                 ]);
                 DB::commit();
                 return Redirect::back()->with(array('success'=>"Password change successfully",'data'=>uniqid()));
             }
         } catch (QueryException $e) {
             DB::rollBack();
             return redirect()
                ->back()
                ->with(array('error'=>"Temporary server error. Try again.",'data'=>uniqid()));
         }
    }
}
