import React, { useEffect } from "react";
import { Button, Card, Col, Form, Input, Row, notification } from "antd";
import { Inertia } from "@inertiajs/inertia";

import AuthLayout from "../../../Shared/AuthLayout";

export default function Login(props) {
    console.log(props, "helo");
    const onFinish = values => {
        Inertia.post(route("admin.login"), values);
    };

    const onFinishFailed = error => {
        console.log(error, "error");
    };

    useEffect(() => {
        props.flash.error &&
            notification.error({
                message: props.flash.error
            });

        props.flash.success &&
            notification.success({
                message: props.flash.success
            });
    }, [props?.flash?.data]);

    return (
        <AuthLayout title="Admin Login">
            <Form
                name="basic"
                initialValues={{
                    remember: true
                }}
                hideRequiredMark
                size="large"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Row
                    style={{
                        display: "flex",
                        justifyContent: "center"
                    }}
                >
                    <Col xl={15} lg={15} xs={22} sm={22}>
                        <Card>
                            <Row>
                                <Col span={24}>
                                    <label>Email</label>
                                </Col>
                                <Col span={24} style={{marginTop:'5px'}}>
                                    <Form.Item
                                        name="email"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter email."
                                            },
                                            {
                                                type: "email",
                                                message:
                                                    "Please enter valid email address"
                                            }
                                        ]}
                                    >
                                        <Input placeholder="Enter Email" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <label>Password</label>
                                </Col>
                                <Col span={24} style={{marginTop:'5px'}}>
                                    <Form.Item
                                        name="password"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter password."
                                            }
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter Password" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        block
                                    >
                                        Submit
                                    </Button>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Form>
        </AuthLayout>
    );
}
