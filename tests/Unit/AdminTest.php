<?php

namespace Tests\Unit;

use App\Model\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use App\Model\Admin;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    protected $admin, $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->admin = factory(Admin::class)->create();
//        $this->admin = Admin::find(3);

        $this->user = factory(User::class)->create(array(
            'password' => Hash::make('laravel'),
        ));
    }

    public function test_admin_show_login()
    {
        $this->withExceptionHandling();
        $response = $this->get(route('login'));
        $response->assertStatus(200);
    }

    public function test_admin_can_login_with_correct_credentials()
    {
        $this->withExceptionHandling();
        Session::start();
        $response = $this->call('POST', route('admin.login'), [
            'email' => $this->admin->first()->email,
            'password' => $this->admin->first()->password,
            '_token' => csrf_token(),
        ]);

        $response->assertStatus(302);
    }

//    public function test_admin_can_login_with_invalid_credentials()
//    {
//        $this->withExceptionHandling();
//        Session::start();
//        $response = $this->post('/admin-login', [
//            'email' => $this->admin->email,
//            'password' => 'invalid',
//            '_token' => csrf_token(),
//        ]);
//        $response->assertSessionMissing('errors');
//    }

    public function test_authenticated_admin_show_dashboard()
    {
        $this->withExceptionHandling();
        $this->actingAs($this->admin);
        $response = $this->get(route('dashboard'));
        $response->assertStatus(302);
    }

    public function test_authenticated_admin_show_profile()
    {
        $this->withExceptionHandling();
        $this->actingAs($this->admin);
        $this->get(route('account.profile'))->assertStatus(302);
    }

    public function test_authenticated_admin_show_change_password()
    {
        $this->withExceptionHandling();
        $this->actingAs($this->admin);
        $this->get(route('account.changePwd'))->assertStatus(302);
    }

    public function test_authenticated_admin_update_profile()
    {
        $this->withExceptionHandling();
        $this->admin->first()->update([
            'first_name' => 'update first name'
        ]);

//        $this->admin->update(array(
//            'first_name' => 'update first name'
//        ));

        $this->actingAs($this->admin)->get(route('account.profile'))->assertStatus(302);
    }

    public function test_authenticated_admin_change_password()
    {
        $this->withExceptionHandling();
        $newPassword = 'new password';

        $this->admin->first()->update(array(
            'current_pwd' => $this->admin->first()->password,
            'new_pwd' => Hash::make($newPassword),
            'confirm_pwd' => Hash::make($newPassword)
        ));

        $this->actingAs($this->admin)->get(route('account.changePwd'))->assertStatus(302);
    }

    public function test_authenticated_admin_can_access_users()
    {
        $this->actingAs($this->admin)->get(route('user'))->assertStatus(302);
    }

    public function test_authenticated_admin_can_active_user_permission()
    {
        $this->user->update(array(
            'status' => 1
        ));

        $this->actingAs($this->admin)->get(route('user'))->assertStatus(302);
    }

    public function test_authenticated_admin_can_inactive_user_permission()
    {
        $this->user->update(array(
            'status' => 0
        ));

        $this->actingAs($this->admin)->get(route('user'))->assertStatus(302);
    }

    public function test_authenticated_admin_can_change_user_password()
    {
        $this->user->update(array(
            'current_pwd' => $this->user->password,
            'new_pwd' => Hash::make('update password'),
            'confirm_pwd' => Hash::make('update password')
        ));

        $this->actingAs($this->admin)->get(route('user'))->assertStatus(302);
    }

    public function test_authenticated_admin_logout()
    {
        $response = $this->actingAs($this->admin)->call('GET', route('admin.logout'));
        $response->assertStatus(302);
    }
}
