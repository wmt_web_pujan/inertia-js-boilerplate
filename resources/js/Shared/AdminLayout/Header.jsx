import React, { useEffect, useState } from "react";
import { Button, Col, Drawer, Menu, Row, Typography } from "antd";
import {
    DesktopOutlined,
    FileOutlined,
    MenuOutlined,
    PieChartOutlined,
    TeamOutlined,
    UserOutlined,
    MenuFoldOutlined
} from "@ant-design/icons";
import { InertiaLink } from "@inertiajs/inertia-react";
const { SubMenu } = Menu;
const { Title, Text } = Typography;
import User from "../../../../public/assets/general/User_placeholder.svg";
import Logo from "../../../../public/assets/general/webmob-logo.svg";

function Header(props) {
    const { admin } = props;
    const [isDesktop, setIsDesktop] = useState(null);
    const [visible, setVisible] = useState(false);
    const [collapsed, setCollapsed] = useState(true);
    const [activeMenu, setActiveMenu] = useState(null);
    const [activeMainMenu, setActiveMainMenu] = useState(null);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    useEffect(() => {
        const width = window.innerWidth;
        setIsDesktop(width);
        setActiveMenu(route().current());
        setActiveMainMenu(
            route()
                .current()
                .split(".")[0]
        );
    }, []);

    useEffect(() => {
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    });

    const handleResize = () => {
        setIsDesktop(window.innerWidth);
    };

    const handleCollapsed = () => {
        setCollapsed(!collapsed);
        props.handleCollapsed(collapsed);
    };

    return (
        <div>
            {isDesktop > 991.98 ? (
                <Row
                    type="flex"
                    justify="center"
                    align="middle"
                    className="header_bg_color"
                >
                    <Col span={24} style={{ padding: "0px 20px" }}>
                        <div className="app_header">
                            <Row
                                type="flex"
                                justify="middle"
                                align="space-between"
                            >
                                <Col
                                    style={{
                                        height: "auto",
                                        display: "flex",
                                        justifyContent: "space-between",
                                        alignItems: "center"
                                    }}
                                >
                                    <div
                                        style={{
                                            width: "182px",
                                            color: "white"
                                        }}
                                    >
                                        <img src={Logo} className="app_logo" />
                                    </div>
                                    <div
                                        onClick={handleCollapsed}
                                        style={{ cursor: "pointer" }}
                                    >
                                        <MenuFoldOutlined
                                            style={{
                                                fontSize: "20px",
                                                color: "white"
                                            }}
                                        />
                                    </div>
                                </Col>
                                <Col
                                    className="app_menu_wrapper"
                                    style={{ height: "auto" }}
                                >
                                    <div className="app_menu">
                                        <Menu
                                            mode="horizontal"
                                            triggerSubMenuAction="click"
                                            style={{
                                                lineHeight: "0px",
                                                backgroundColor: "transparent"
                                            }}
                                            defaultSelectedKeys={[activeMenu]}
                                            defaultOpenKeys={[activeMainMenu]}
                                        >
                                            <SubMenu
                                                style={{ margin: "0px 10px" }}
                                                title={
                                                    <div
                                                        style={{
                                                            display: "flex",
                                                            alignItems: "center"
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                position:
                                                                    "relative",
                                                                width: "35px",
                                                                height: "35px",
                                                                overflow:
                                                                    "hidden",
                                                                borderRadius:
                                                                    "50%",
                                                                display: "flex",
                                                                padding: "2px"
                                                            }}
                                                        >
                                                            {admin?.profile_pic_path ? (
                                                                <img
                                                                    src={
                                                                        admin?.profile_pic_path
                                                                    }
                                                                    alt="avatar"
                                                                    style={{
                                                                        display:
                                                                            "inline",
                                                                        height:
                                                                            "auto",
                                                                        width:
                                                                            "100%",
                                                                        margin:
                                                                            "auto 0"
                                                                    }}
                                                                />
                                                            ) : (
                                                                <img
                                                                    src={User}
                                                                    alt="avatar"
                                                                    style={{
                                                                        display:
                                                                            "inline",
                                                                        height:
                                                                            "auto",
                                                                        width:
                                                                            "100%",
                                                                        margin:
                                                                            "auto 0"
                                                                    }}
                                                                />
                                                            )}
                                                        </div>
                                                        <Title
                                                            style={{
                                                                fontSize:
                                                                    "14px",
                                                                marginTop:
                                                                    "0.5em",
                                                                marginLeft:
                                                                    "5px",
                                                                color: "white",
                                                                textTransform:
                                                                    "capitalize"
                                                            }}
                                                        >
                                                            {admin?.first_name}{" "}
                                                            {admin?.last_name}
                                                        </Title>
                                                    </div>
                                                }
                                            >
                                                <Menu.Item key="account.profile">
                                                    <InertiaLink
                                                        href={route(
                                                            "account.profile"
                                                        )}
                                                    >
                                                        Profile
                                                    </InertiaLink>
                                                </Menu.Item>
                                                <Menu.Item key="account.changePwd">
                                                    <InertiaLink
                                                        href={route(
                                                            "account.changePwd"
                                                        )}
                                                    >
                                                        Change Password
                                                    </InertiaLink>
                                                </Menu.Item>
                                                <Menu.Item key="account.logout">
                                                    <InertiaLink
                                                        href={route(
                                                            "admin.logout"
                                                        )}
                                                    >
                                                        Log Out
                                                    </InertiaLink>
                                                </Menu.Item>
                                            </SubMenu>
                                        </Menu>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            ) : (
                <div className="app_header">
                    <Row
                        type="flex"
                        justify="center"
                        align="middle"
                        className="app_menu_wrapper"
                    >
                        <Col span={22} className="app_header_mobile">
                            <InertiaLink
                                href="website"
                                style={{ color: "white" }}
                            >
                                <img src={Logo} className="app_logo" />
                            </InertiaLink>
                            <div
                                style={{
                                    display: "flex",
                                    float: "right",
                                    flexDirection: "row"
                                }}
                            >
                                <div>
                                    <Button
                                        type="link"
                                        onClick={showDrawer}
                                        style={{
                                            padding: "8px 8px",
                                            color: "white"
                                        }}
                                    >
                                        <MenuOutlined />
                                    </Button>
                                </div>
                            </div>
                        </Col>
                        <Col>
                            <Drawer
                                placement="left"
                                closable={false}
                                onClose={onClose}
                                visible={visible}
                                bodyStyle={{ padding: 0 }}
                                className="drawer_color"
                            >
                                <Menu
                                    defaultSelectedKeys={["1"]}
                                    mode="inline"
                                    theme="dark"
                                >
                                    <Menu.Item
                                        key="1"
                                        icon={<PieChartOutlined />}
                                    >
                                        Option 1
                                    </Menu.Item>
                                    <Menu.Item
                                        key="2"
                                        icon={<DesktopOutlined />}
                                    >
                                        Option 2
                                    </Menu.Item>
                                    <SubMenu
                                        key="sub1"
                                        icon={<UserOutlined />}
                                        title="User"
                                    >
                                        <Menu.Item key="3">Tom</Menu.Item>
                                        <Menu.Item key="4">Bill</Menu.Item>
                                        <Menu.Item key="5">Alex</Menu.Item>
                                    </SubMenu>
                                    <SubMenu
                                        key="sub2"
                                        icon={<TeamOutlined />}
                                        title="Team"
                                    >
                                        <Menu.Item key="6">Team 1</Menu.Item>
                                        <Menu.Item key="8">Team 2</Menu.Item>
                                    </SubMenu>
                                    <Menu.Item key="9" icon={<FileOutlined />}>
                                        Files
                                    </Menu.Item>
                                </Menu>
                            </Drawer>
                        </Col>
                    </Row>
                </div>
            )}
        </div>
    );
}

export default Header;
