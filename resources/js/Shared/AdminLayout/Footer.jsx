import { Col, Row } from "antd";
import { InertiaLink } from "@inertiajs/inertia-react";
import React from "react";

export default function Footer() {
    return (
        <Row>
            <Col xs={24} sm={24} lg={7} style={{ textAlign: "center" }}>
                <span>Copyright © {new Date().getFullYear()} Admin </span>
            </Col>
            <Col
                xs={24}
                sm={24}
                lg={10}
                style={{
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "center"
                }}
            >
                <InertiaLink href="#" style={{ margin: "0 15px" }}>
                    Privacy Policy
                </InertiaLink>
                <InertiaLink href="#" style={{ margin: "0 15px" }}>
                    Terms of Service
                </InertiaLink>
                <InertiaLink href="#" style={{ margin: "0 15px" }}>
                    Cookies Policy
                </InertiaLink>
            </Col>
        </Row>
    );
}
