import React, {useEffect, useState} from "react";
import {Layout, Menu} from "antd";
import {InertiaLink} from "@inertiajs/inertia-react";
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    UserOutlined
} from "@ant-design/icons";

const {Sider} = Layout;
const {SubMenu} = Menu;

function AdminSider(props) {
    const {admin} = props;
    const [isDesktop, setIsDesktop] = useState(null);
    const [collapsed, setCollapsed] = useState(false);
    const [activeMenu, setActiveMenu] = useState(null);
    const [activeMainMenu, setActiveMainMenu] = useState(null);

    useEffect(() => {
        const width = window.innerWidth;
        setIsDesktop(width);
        setActiveMenu(route().current());
        setActiveMainMenu(route().current().split(".")[0]);
    }, []);

    useEffect(() => {
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    });

    useEffect(() => {
        setCollapsed(props.collapsed);
    }, [props.collapsed]);

    const handleResize = () => {
        setIsDesktop(window.innerWidth);
    };

    return (
        <div className="sider_color">
            {isDesktop > 991.98 ? (
                <Sider collapsible collapsed={collapsed} trigger={null}>
                    <Menu
                        theme="dark"
                        defaultSelectedKeys={[activeMenu]}
                        defaultOpenKeys={[activeMainMenu]}
                        mode="inline"
                    >
                        {
                            (admin?.role?.map(x => x.name).includes("dashboard") || admin?.role?.map(x => x.name).includes("super admin")) ?
                                <Menu.Item key="dashboard" icon={<PieChartOutlined/>}>
                                    <InertiaLink href={route("dashboard")}>
                                        DashBoard
                                    </InertiaLink>
                                </Menu.Item> : null
                        }

                        {
                            (admin?.role?.map(x => x.name).includes("user") || admin?.role?.map(x => x.name).includes("super admin")) ?
                                <Menu.Item key="user" icon={<UserOutlined/>}>
                                    <InertiaLink href={route("user")}>User</InertiaLink>
                                </Menu.Item> : null
                        }
                        {
                            admin?.role?.map(x => x.name).includes("super admin") &&
                            <SubMenu
                                key="acl"
                                icon={<TeamOutlined/>}
                                title="Manage Role"
                            >
                                <Menu.Item key="acl.role">
                                    <InertiaLink href={route("acl.role")}>
                                        Role
                                    </InertiaLink>
                                </Menu.Item>
                                <Menu.Item key="acl.admin">
                                    <InertiaLink href={route("acl.admin")}>
                                        Admin
                                    </InertiaLink>
                                </Menu.Item>
                            </SubMenu>
                        }
                        {
                            (admin?.role?.map(x => x.name).includes("files") || admin?.role?.map(x => x.name).includes("super admin")) ?
                                <Menu.Item key="9" icon={<FileOutlined/>}>
                                    Files
                                </Menu.Item> : null
                        }

                    </Menu>
                </Sider>
            ) : (
                ""
            )}
        </div>
    );
}

export default AdminSider;
