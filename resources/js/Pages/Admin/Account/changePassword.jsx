import AdminLayout from "../../../Shared/AdminLayout/index";
import React, { useRef, useEffect } from "react";
import { Inertia } from "@inertiajs/inertia";
import {
    Button,
    Card,
    Col,
    Form,
    Input,
    Row,
    Typography,
    notification,
    Breadcrumb
} from "antd";
import { InertiaLink } from "@inertiajs/inertia-react";
import { HomeOutlined } from "@ant-design/icons";

const { Title } = Typography;

export default function Password(props) {
    const formRef = useRef(null);
    const onFinish = values => {
        formRef.current.resetFields();
        Inertia.post(route("change-admin-pwd"), values);
    };

    const onFinishFailed = values => {
        console.log(values, "error");
    };

    useEffect(() => {
        props.flash.error &&
            notification.error({
                message: props.flash.error
            });

        props.flash.success &&
            notification.success({
                message: props.flash.success
            });
    }, [props?.flash?.data]);

    return (
        <AdminLayout title="Change Password">
            <Row style={{ marginTop: "2.0em" }}>
                <Col span={24}>
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <InertiaLink href={route("dashboard")}>
                                <HomeOutlined />
                            </InertiaLink>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <span className="text_app_primary_color">
                                Change password
                            </span>
                        </Breadcrumb.Item>
                    </Breadcrumb>
                </Col>
            </Row>
            <Form
                ref={formRef}
                layout="vertical"
                name="basic"
                initialValues={{
                    remember: true
                }}
                size="large"
                hideRequiredMark
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                style={{ marginTop: "2.0em" }}
            >
                <Row
                    type="flex"
                    justify="center"
                    align="middle"
                    style={{ height: "calc(100vh - 300px)" }}
                >
                    <Col xl={12} lg={12} xs={22} sm={22}>
                        <Card>
                            <Row type="flex" justify="center" align="middle">
                                <Col span={24}>
                                    <label>Enter Current Password</label>
                                </Col>
                                <Col span={24} style={{ marginTop: "5px" }}>
                                    <Form.Item
                                        name="current_pwd"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter current password."
                                            }
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter Current Password" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <label>Enter New Password</label>
                                </Col>
                                <Col span={24} style={{ marginTop: "5px" }}>
                                    <Form.Item
                                        name="new_pwd"
                                        style={{ marginBottom: "12px" }}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter new password."
                                            },
                                            {
                                                pattern: /^(?=.*\d)(?=.*[0-9])(?=.*[a-zA-Z]).{8,}/,
                                                message:
                                                    "The password should be 8 characters atleast and should have atleast one number."
                                            }
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter New Password" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <label>Enter Password Again</label>
                                </Col>
                                <Col span={24} style={{ marginTop: "5px" }}>
                                    <Form.Item
                                        name="confirm_pwd"
                                        style={{ marginBottom: "12px" }}
                                        dependencies={["new_pwd"]}
                                        rules={[
                                            {
                                                required: true,
                                                message:
                                                    "Please enter password again."
                                            },
                                            ({ getFieldValue }) => ({
                                                validator(rule, value) {
                                                    if (
                                                        !value ||
                                                        getFieldValue(
                                                            "new_pwd"
                                                        ) === value
                                                    ) {
                                                        return Promise.resolve();
                                                    }
                                                    return Promise.reject(
                                                        "password and confirm password do not match."
                                                    );
                                                }
                                            })
                                        ]}
                                    >
                                        <Input.Password placeholder="Enter Password Again" />
                                    </Form.Item>
                                </Col>
                                <Col span={24}>
                                    <div
                                        style={{
                                            float: "right"
                                        }}
                                    >
                                        <Button
                                            type="link"
                                            style={{ margin: "0px 10px" }}
                                            size="middle"
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="primary"
                                            htmlType="submit"
                                            size="middle"
                                        >
                                            Update
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Form>
        </AdminLayout>
    );
}
