<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Model\Admin::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
//        'password' => '$2y$10$DE2rva2v8gOq7Yz9ORDuE.60.P6ZqLrmAtJE7dctgVUSmGGlR7BRO',
        'password' => 'secret',
        'remember_token' => Str::random(10),
    ];
});
